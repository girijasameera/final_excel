from django.conf.urls import patterns, url

from quiz import views

urlpatterns = patterns(
        '',

        url(r'^import/', views.import_data, name="import"),

)

