from django.shortcuts import render_to_response
from django.http import HttpResponseBadRequest, HttpResponse
from django import forms
from django.template import RequestContext
import quiz.models
import xlrd


class UploadFileForm(forms.Form):
    file = forms.FileField()


def import_data(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)

        if form.is_valid():
            input_file = request.FILES.get('file')
            book = xlrd.open_workbook(file_contents=input_file.read())
            sheet = book.sheet_by_index(0)
            for row_num in range(1, sheet.nrows):
                row = sheet.row_values(row_num)
                q = quiz.models.Question.objects.create(question_text=row[0], answer=row[5])
                q.save()

                for i in (1, 4):
                    c = quiz.models.Choice.objects.create(question=q, choice_text=row[i])
                    c.save()
            return HttpResponse("OK", status=200)
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render_to_response(
            'upload_form.html',
            {
                'form': form,
                'title': 'Excel upload',
                'header': 'Please upload excel file:'
            },
            context_instance=RequestContext(request))
