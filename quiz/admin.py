from django.contrib import admin
import quiz.models
# Register your models here.
admin.site.register(quiz.models.Question)
admin.site.register(quiz.models.Choice)